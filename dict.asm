%define size 8
extern string_equals

global find_word

section .text

; Функция ищёт ключ в словаре и, если найден, то возвращает адрес начала, иначе вернёт 0.
; Функция принимает указатель на нуль-терминированную строку и указаткль на начало словаря.

find_word:
.search:
    add rsi, size ; указатель
    push rdi
    push rsi
    call string_equals
    pop rsi
    pop rdi
    cmp rax, 1
    je .found ; если нашли
    mov rsi, [rsi-size] ; смотрит на следующий элемент
    cmp rsi, 0
    jne .search 
.end:
    xor rax, rax
    ret
.found:
    mov rax, rsi
    ret      

