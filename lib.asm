global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy


section .text


; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .lengthcheck:
        cmp byte [rdi+rax], 0x0  ; проверяем на ноль-терминатор
        je .end  ; заканчиваем подсчет длины
        inc rax
        jmp .lengthcheck  ; продолжаем проверку
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length ; вызываем для посчета длины
    mov rsi, rdi
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall ; вызываем write
    ret


; Принимает код символа и выводит его в stdout
print_char:
    push rdi ; кладем в стек
    mov rsi, rsp ; вершина стека, которую будем печатать
    mov rdx, 1 ; длина равна 1
    mov rax, 1
    mov rdi, 1
    syscall
    pop rdi
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA; переводим строку
    call print_char 
    ret


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, 0
        mov rcx, 0
        mov r9, 10 ; делитель
        dec rsp
        mov [rsp], al
        mov rax, rdi

    .separate:
        xor rdx, rdx
        div r9
        add dl, '0'
        dec rsp
        mov [rsp], dl
        inc rcx
        cmp rax, 0
        jne .separate

        mov rdi, rsp ; Печатаем строку
        push rcx
        call print_string
        pop rcx
        add rsp, rcx
        inc rsp
        ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
        jns print_uint
        push rdi
        mov rdi, '-'  ; minus
        call print_char
        pop rdi
        neg rdi
    .print_uint:
        call print_uint
        ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    xor r8, r8
    .check:
        mov r8b, byte [rdi+rcx]  ; rdi -первая строку
        cmp byte [rsi+rcx], r8b  ; rsi -вторая строку
        jne .no
        inc rcx
        cmp r8b, 0x0 ; нуль-терминатор
        jne .check
    .yes:
        mov rax, 1 ; если равны - 1, иначе 0
        ret
    .no:
        xor rax, rax
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, 0
    mov rdi, 0
    push 0 ; буфер
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax ; прочитанный символ
    ret 


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    mov r8, 0 ; счетчик
        mov r9, 0 ; длина
        mov rcx, 0 ; флаг, устанавливается, если символ не пробел/таб
    .read:
        push rdi
        push rsi
        push rcx
        call read_char ;вызываем чтение по символу
        pop rcx
        pop rsi
        pop rdi

        cmp al, 0x9 ; таб
        je .space
        cmp al, 0xA ; перевод строки
        je .space
        cmp al, 0x20 ; пробел
        je .space
        mov rcx, 1 ; начало строки

    .bufferwork:
        cmp rsi, r9 ; проверяем буфер
        jle .error
        mov [rdi + r9], rax ; записываем в буффер
        cmp rax, 0
        je .ok
        inc r9
        jmp .read

    .space:
        cmp rcx, 0
        je .read
        xor rax, rax
        jmp .bufferwork

    .ok:
        mov byte [rdi + r9], 0 ; терминирующий ноль
        mov rax, rdi
        mov rdx, r9
        ret

    .error:
        mov rax, 0
        ret


 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rdx, rdx
    xor rax, rax
    xor r8, r8
    .parse:
        mov r8b, byte [rdi+rdx]
        cmp r8, 0x0 ; нуль-терминатор
        je .end ; конец числа
        cmp r8, 0x30 ; 0
        jb .end
        cmp r8, 0x39 ; 9
        ja .end
        sub r8, 0x30 ;получаем число 0
        imul rax, 10
        add rax, r8
        inc rdx
        jmp .parse 
    .end:
        ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '-' ; проверка на знак
        je .neg
        call parse_uint
        ret 
	.neg:
		inc rdi
		call parse_uint
		neg rax
		inc rdx ; инкремент из-за минуса
		ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    .copy:
        cmp rax, rdx
	je .error  ; если переполнение буфера
	mov r11, [rdi+rax]
	mov [rsi+rax], r11
	cmp byte [rdi+rax], 0x0 ; проверяем на ноль-терминатор
	je .end ; конец строки
	inc rax
	jmp .copy ; копируем дальше
    .error:
        xor rax, rax
    .end:
        ret
