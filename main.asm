%include 'words.inc'

%define buffSize 256

section .data

input: db "Введите ключ: ", 0xA, 0
failed: db "Ключ не найден", 0xA, 0
keylong: db "Значение превышает 255 символов", 0xA, 0
found: db "Найдено значение: ", 0xA, 0
buffer: times buffSize db 0 

global _start

extern find_word
extern read_word
extern print_string
extern print_newline
extern exit


section .text

_start: 
        mov rdi, input
        call print_string
        mov rdi, buffer
        mov rsi, buffSize
        call read_word
        cmp rax, 0 ; если rax = 0, то ключ больше
        je .printiflong

        push rdx ; длина ключа
        mov rdi, buffer ; кладем в буфер
        mov rsi, count ; конец словаря
        call find_word ; ищем
        cmp rax, 0 ; если rax = 0, то ключ не найден
        je .printnotfound

        pop rdx
        add rax, rdx
        inc rax
        push rax ; начальный адрес значения
        je .printfound

    .printfound: ; нашли
        mov rdi, found
        call print_string
        pop rdi
        call print_string ; выводим значение
        call print_newline
        mov rdi, 0
        call exit

    .printnotfound:;не нашли
        mov rdi, failed
        call print_string
        mov rdi, 0
        call exit

    .printiflong: ; ключ больше 255
        mov rdi, keylong
        call print_string
        mov rdi, 0
        call exit
